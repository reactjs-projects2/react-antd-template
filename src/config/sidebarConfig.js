const sideMenuConfig = [
    {
      "routes": [
        "/app/home",
      ],
      "subject": "Dashboard",
      "icon": "dollar",
      "submenu": false
    }  
  ]

  export default {
      sideMenuConfig
  }
import { combineReducers } from 'redux'

import Menu from './menu/reducer'

/**
 *  Combine reducers
 */
export default combineReducers({
  Menu,
});

import React, { Component } from 'react';
import { Route, Switch, HashRouter, Link } from 'react-router-dom';
import { Layout } from 'antd';
import { connect } from 'react-redux';
import HeaderContext from 'contexts/HeaderContext';
import SideBar from 'components/SideMenu/SideBar';
import TopBar from 'components/TopBar/TopBar';
import Loading from 'components/Loading/Loading';
import Retry from 'components/Loading/LoaderRetry';
import ModuleLoader from 'services/splitLoader';
import Expired from 'pages/Exceptions/Exceptions';
import './app.less';

import sidebarConfig from 'config/sidebarConfig'

//We will use react-loadable for code splitting.

function Loader(props) {
  if (props.error) {
    return <Retry message='Error! Please Click to Retry' retry={props.retry} />;
  } else if (props.timedOut) {
    return <Retry message='Loading..... Taking a long time?' retry={props.retry} />;
  } else if (props.pastDelay) {
    return <Loading />;
  } else {
    return null;
  }
}

const Home = ModuleLoader({
  loader: () => import('pages/HomePages/commonHome'),
  loading: Loader
});


const { Footer, Content } = Layout;

class App extends Component {

  state = {
    abilities: sidebarConfig.sideMenuConfig,
    loading: false,
    routes: {},
    totalSubmenus: 0,
    header: 'Dashboard',
  };

  constructor(props){
    super(props);
    this.idleTimer = null;
  }
  
  setHeader = (header) => {
    this.setState({ header });
  }

  render() {
    const { collapsed } = this.props;

    if (this.state.loading) {
      return <Loading />;
    }

    
    return (
      <HashRouter>        
        <Layout>
          <SideBar abilities={this.state.abilities} subMenus={this.state.routes} totalSubmenus={this.state.totalSubmenus} setHeader={this.setHeader}/>
          <Layout style={{ marginLeft: (collapsed) ? 80 : 256, minHeight: '100vh' }}>
            <HeaderContext.Provider value={this.state.header}>
              <TopBar />
            </HeaderContext.Provider>            
            <Content className="content">
              <Switch>
                <Route exact path="/app/home" component={Home}/>
                <Route component={Expired} />
              </Switch>
            </Content>
            <Footer style={{ textAlign: 'center' }}>©2019 Protolive <br/>
            {/* <Link to="/app/policy">Terms & Conditions</Link> */}
            </Footer>
          </Layout>
        </Layout>
      </HashRouter>
    );
  }
}

function mapStateToProps(state) {
  return {
    collapsed: state.Menu.collapsed
  }
}

export default connect(mapStateToProps)(App);
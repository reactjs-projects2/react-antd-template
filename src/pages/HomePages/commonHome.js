import React, { Component } from 'react'
// import { Redirect } from 'react-router-dom';
import { Layout } from 'antd';
import PageHead from 'components/PageHeader/PageHeader';
import Sketch from 'components/Sketch'

const { Content } = Layout;


export default class commonHome extends Component {
    render() {
        return (
            <Layout>
                <Content>
                    <PageHead title='Welcome!'>                                          
                        <div style={{ padding: 24, background: '#fff', minHeight: '82vh' }}> 
                        <Sketch />
                        </div>
                    </PageHead>
                </Content>
            </Layout>
        )
    }
}

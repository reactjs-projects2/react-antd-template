import React, { Component } from 'react';
import { Card, Form, Icon, Input, Button, Spin, Alert} from "antd";
import { Redirect } from 'react-router-dom';
import { parseQuery } from 'services/queryParser';
import { inviteSignIn } from 'services/aws_services';
import Loading from 'components/Loading/Loading';
import jwt_decode from 'jwt-decode';
import './Login.less';
import './Login.css';

const spinIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

class Verify extends Component {

    state = {
        confirmDirty: false,
        //the URL will contain a JWT token (/confirm?cd=...) which contains Expiration time, username and one-time security code.
        //delegateToken will contain an object with {user: , code: , exp: } exp will be time in Epoch format
        delegateToken: parseQuery(this.props.location.search),
        url: this.props.location.search,
        // expiry: '',
        loading: true,
        expired: false,
        submitted: false,
        scode: '',
        err: ''
    };

    componentDidMount(){
        
        if(this.state.delegateToken.cd){    
            
            try{
                //the statement below may fail if malformed jwt is provided
                const decoded = jwt_decode(this.state.delegateToken.cd);  
                const { user, exp } = decoded;
                const diff = exp - Math.floor(Date.now() / 1000);
                const isExpired = Math.floor(diff / 3600) <= 0;
                
                const securityCode = this.state.url.split("&scd=")[1];
                console.log("Sec code: ",securityCode);
                this.setState({
                    // expiry: exp,
                    expired: isExpired,
                    loading: false,
                    scode: securityCode //decodeURIComponent(this.state.delegateToken.scd)
                }, () => {                    
                    this.props.form.setFieldsValue({
                        username: user,                                                                     
                    });
                });
            }catch(err){
                this.setState({                    
                    expired: true,
                    loading: false
                });
            }                    
        }else{
            this.setState({                    
                expired: true,
                loading: false
            });
        }
        
    }

    handleSubmit = (e) => {
        e.preventDefault();
        
        this.props.form.validateFields((err, values) => {
          if (!err) {
            
            this.setState({ submitted: true }, async () => {
                console.log('Received values of form: ', values);

                try{
                    await inviteSignIn(values.username, this.state.scode, values.password);
                    this.props.history.push({
                        pathname: '/app/invoice/all'
                    });
                }catch(err){
                    this.setState({ err: err });
                }
            });
          }
        });
      }
    
    strongValidator = (rule,value,callback) => {
        const reg = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})");
        const lower = new RegExp("^(?=.*[a-z])");
        const upper = new RegExp("^(?=.*[A-Z])");
        const numeric = new RegExp("^(?=.*[0-9])");
        const symbol = new RegExp("^(?=.[!@#$%^&])");
        const length = new RegExp("^(?=.{8,})");
        if(value && !reg.test(value)){
            callback(
            <div>
                <ul style={{textAlign: 'left', color: 'grey'}}>
                    {
                        (!length.test(value))?
                        <li>Must be atleast 8 Characters</li>
                        :undefined
                    }
                    {
                        (!upper.test(value))?
                        <li>Must contain atleast 1 uppercase character</li>
                        :undefined
                    }                    
                    {
                        (!lower.test(value))?
                        <li>Must contain atleast 1 lowercase character</li>
                        :undefined
                    }
                    {
                        (!symbol.test(value) || !numeric.test(value))?
                        <li>Must contain atleast 1 number and 1 special character</li>
                        :undefined
                    }
                    
                </ul>
            </div>);
        }else{
            callback();
        }
    }

    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue("password")) {
          callback("Password doesn't match!");
        } else {
          callback();
        }
    };

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
          form.validateFields(["confirm"], { force: true });
        }
        if(value && form.getFieldValue("confirm") && value !== form.getFieldValue("confirm")){
            form.validateFields(["confirm"], { force: false });
        }
        callback();
    };

    render() {
    const { getFieldDecorator } = this.props.form;

    if(this.state.loading){
        return <Loading/>;        
    }

    if(this.state.expired){
        return <Redirect to={{
                                pathname: `/app/invite/expired`,
                                state: {code: 512 }
                            }} />;
    }

    return (
        <div className="container"> 
        <div className="login">
            <Card
            cover={
                <img
                alt="example"
                src="https://s7d1.scene7.com/is/image/PETCO/services-032218-pet-sitting-302w-185h-d"
                />
            }
            bordered={false}    
            bodyStyle={{padding: '0'}}        
            >            
                <Card bordered={false} title="Confirm Registration" style={{textAlign: 'center'}} bodyStyle={{width: '100%'}}>
                    <Spin indicator={spinIcon} tip="Logging In" spinning={this.state.submitted}>
                        <Form onSubmit={this.handleSubmit} className="login-form" hideRequiredMark>
                            <Form.Item>
                                {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Please enter your username' }],
                                })(
                                <Input
                                    disabled
                                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    placeholder="Username"
                                />,
                                )}
                            </Form.Item>                        
                            <Form.Item hasFeedback>
                                {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please enter your password' },
                                        {
                                            validator: this.validateToNextPassword
                                        },
                                        {
                                            validator: this.strongValidator
                                        }]
                                })(
                                <Input                        
                                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    type="password"
                                    placeholder="New Password"
                                />,
                                )}
                            </Form.Item>
                            <Form.Item hasFeedback>
                                {getFieldDecorator('confirm', {
                                rules: [{ required: true, message: 'Please enter your password' },
                                        {
                                            validator: this.compareToFirstPassword
                                        }],
                                })(
                                <Input                        
                                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    type="password"
                                    placeholder="Confirm Password"
                                />,
                                )}
                            </Form.Item>
                            {/* <Form.Item>
                                {getFieldDecorator('scode')(
                                <Input
                                    hidden
                                />,
                                )}
                            </Form.Item> */}
                            <Form.Item>                                                                            
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Confirm
                                </Button>       
                            </Form.Item>                
                        </Form>                
                        {
                            (!!this.state.err)?
                            <Alert message={this.state.err} type="error" />
                            :undefined
                        }
                    </Spin>
                </Card>
            </Card>
        </div>
        </div>
    )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Verify);

export default WrappedNormalLoginForm;
import React from 'react'
import {Col, Row, Divider, Typography } from 'antd'
import './index.less';

const Title = Typography.Title;
const Section = (props) => {
    return(
        <div className="section-container" style={{...props.style}}>
        {
            (props.title)&&
            <Row type="flex" justify="start" >
                <Col span={24}>
                    <Title level={4} style={{fontWeight: '400'}}>{props.title}</Title>
                    <Divider />
                </Col>
            </Row>
        }
            
        {
            props.children
        }
    </div>
    )
}

Section.defaultProps = {
    minHeight: 0
}

export default Section
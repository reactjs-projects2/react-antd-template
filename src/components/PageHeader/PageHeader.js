import React, { Component } from 'react';
import { PageHeader, Typography } from 'antd';
import './index.less';

const { Title } = Typography;

export default class PageHead extends Component {
  render() {
    const { children, title, routes, extra, content, extraContent } = this.props;
    return (
      <div style={{ margin: '-24px -24px 0' }} className="main" >
        <PageHeader
            wide={false}
            title= {
              <Title
                level={4}
                style={{
                  fontWeight: '400',
                  marginBottom: 10,
                }}
              >
                {title}
              </Title>
            }
            className="pageHeader"
            breadcrumb={{routes}}
            extra={extra}
        >
          {/* <div className="main"> */}
            <div>
              {content && <div >{content}</div>}
              {extraContent && <div>{extraContent}</div>}
            </div>
          {/* </div> */}
        </PageHeader>
        {children ? (
          <div className='children-content'>
            {children}
          </div>
        ) : null}
      </div>
    )
  }
}

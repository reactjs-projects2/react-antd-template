import React from 'react';
import { Button } from 'antd';

export default function LoaderRetry(props) {
  return (
        <div style={{
            width: '100%',
            height: '100%',
            margin: 'auto',
            paddingTop: 50,
            textAlign: 'center',
            }}>
            <span>{props.message}</span><br/>         
            <Button type="danger" shape="round" icon="redo" onClick={props.retry}>Retry</Button>
        </div>
  )
}

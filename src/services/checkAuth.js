import React, {useState,useEffect} from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Spin } from "antd";

const CheckAuth = ({ component: Component, ...rest }) => {
    const [isAuthenticated, setAuth ] = useState(false);
    const [isChecking, setCheck] = useState(true);

    const fetchAuth = async () => {

        /**
         * this logic needs to be implemented and loggedIn session needs to validate 
         * Currently hardcoding to true 
         */
        setAuth(true);             
        setCheck(false);
        // try{
        //     const res = await getUserSession();
        //     setAuth(res.isAuthenticated);             
        //     setCheck(false);          
        // }catch(err){            
        //     setAuth(false); 
        //     setCheck(false);
        // }
             
    }

    useEffect(() => {           
        fetchAuth();        
    }, []);

    if(isChecking){
        return (
            <div style={{
                        width: '100%',
                        height: '100%',
                        margin: 'auto',
                        paddingTop: 50,
                        textAlign: 'center',
                        }}>
                <Spin size="large" />
            </div>
        )
    }

    return (
        <Route {...rest} render={(props) => {
                return (isAuthenticated === true
                ? <Component {...props} />
                : <Redirect to={{pathname: '/', state: { from: props.location }}}/>
            )}
        } />
    )
}

export default CheckAuth;
